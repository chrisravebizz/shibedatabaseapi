package com.rave.shibe.di

import android.content.Context
import com.rave.shibe.model.local.ShibeDatabase
import com.rave.shibe.model.local.dao.ShibeDao
import com.rave.shibe.model.remote.ShibeService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    // Module is a scope were we can create many instances that we want available throughout our application

    @Provides
    @Singleton
    fun providesShibeService(): ShibeService = ShibeService.getInstance()

    @Provides
    @Singleton
    fun providesShibeDatabase(@ApplicationContext context: Context): ShibeDatabase =
        ShibeDatabase.getInstance(context)
}
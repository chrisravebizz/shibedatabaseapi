package com.rave.shibe.model.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Shibe(

    // Data Entities represent tables in our app's database

    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val url: String,
    var isLiked: Boolean = false
)
package com.rave.shibe.model

import android.util.Log
import com.rave.shibe.model.local.ShibeDatabase
import com.rave.shibe.model.local.entity.Shibe
import com.rave.shibe.model.remote.ShibeService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "ShibeRepo"
@Singleton
class ShibeRepo @Inject constructor(
    private val shibeService: ShibeService,
    private val shibeDatabase: ShibeDatabase
) {

    suspend fun getShibes() = withContext(Dispatchers.IO) {
        val cachedShibes: List<Shibe> = shibeDatabase.shibeDao().getAll()

        return@withContext cachedShibes.ifEmpty {
            val shibeUrls: List<String> = shibeService.getShibes()
            Log.d(TAG, "shibeUrls size is ${shibeUrls.size}")
            val shibes: List<Shibe> = shibeUrls.map { Shibe(url = it) }
            Log.d(TAG, "shibes size is ${shibes.size}")
            shibeDatabase.shibeDao().insert(shibes)
            Log.d(TAG, "shibes size is ${shibeDatabase.shibeDao().getAll().size}")
            return@ifEmpty shibes
        }
    }

    suspend fun updateShibeDao(shibe: Shibe) {
        shibeDatabase.shibeDao().update(shibe)
    }
}
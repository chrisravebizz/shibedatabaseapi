package com.rave.shibe.viewmodel

import androidx.lifecycle.*
import com.rave.shibe.model.ShibeRepo
import com.rave.shibe.model.local.entity.Shibe
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ShibeViewModel @Inject constructor(
    private val repo: ShibeRepo
    ) : ViewModel() {
    fun updateShibeDao(shibe: Shibe) {
        viewModelScope.launch {
            repo.updateShibeDao(shibe)
        }
    }

    val state: LiveData<ShibeState> = liveData {
        emit(ShibeState(isLoading = true))
        val shibes = repo.getShibes()
        emit(ShibeState(shibes = shibes))
    }

    data class ShibeState(
        val isLoading: Boolean = false,
        val shibes: List<Shibe> = emptyList()
    )
//
//    class ShibeViewModelFactory(
//        private val shibeRepo: ShibeRepo
//    ) : ViewModelProvider.NewInstanceFactory() {
//
//        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
//            return ShibeViewModel(shibeRepo) as T
//        }
//    }
}
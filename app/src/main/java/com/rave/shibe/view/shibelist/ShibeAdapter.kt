package com.rave.shibe.view.shibelist

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.rave.shibe.databinding.ItemShibeBinding
import com.rave.shibe.model.local.entity.Shibe
import com.rave.shibe.viewmodel.ShibeViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ShibeAdapter(
    private val shibes: List<Shibe>, private val viewModel : ShibeViewModel
) : RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ) = ShibeViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        val url = shibes[position]
        holder.loadShibeImage(url, viewModel)
    }

    override fun getItemCount() = shibes.size

    class ShibeViewHolder(
        private val binding: ItemShibeBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadShibeImage(shibe: Shibe, shibeViewModel: ShibeViewModel) {
            binding.imgItem.load(shibe.url)
            binding.favoriteImg.isVisible = shibe.isLiked
            binding.imgItem.setOnClickListener {
                shibe.isLiked = !shibe.isLiked
                CoroutineScope(Dispatchers.IO).launch { shibeViewModel.updateShibeDao(shibe) }
                binding.favoriteImg.isVisible = shibe.isLiked
            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemShibeBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { binding -> ShibeViewHolder(binding) }
        }
    }
}
